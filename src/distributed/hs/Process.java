package distributed.hs;

public class Process implements Runnable {
	public int number;
	public int id;
	public Channel ltCh;
	public Channel rtCh;
	public int phase = 0;
	public int leader = 0;
	public int currentRound;
	public int returnedMessages = 2;
	public boolean isRoundComplete = false;


	public enum CHANNEL_DIRECTION {
		LEFT, RIGHT
	}

	/*
	 * number - process index number 
	 * id - Process ID, it should be unique 
	 * left - Left Channel 
	 * right - Right Channel
	 */
	public Process(int number, int id, Channel left, Channel right) {
		super();
		this.number = number;
		this.id = id;
		this.ltCh = left;
		this.rtCh = right;
		this.currentRound = 0;
	}
	public int findHopCount = 0;

	private void processIncomingMessage(Message msg, CHANNEL_DIRECTION channelDir) {
		if (msg.direction == DIRECTION.OUT) {
			if (msg.processId == this.id) {
				this.leader = 1;
				return;
			} else if (msg.processId < id) {
				System.out.println("Process " + msg.processId + " has lost to " + this.id);	// Dropping the message
			} else if (msg.processId > id) {
				if (msg.hopcount == 1)	returnMsg(msg, channelDir);	// Reverse the direction
				else	forwardMsg(msg, channelDir);	// Keep forwarding the message
			}
		} else if (msg.direction == DIRECTION.IN) {
			if (msg.processId == this.id)	returnedMessages++;
			else	forwardMsg(msg, channelDir);	// Fwd the msg till it reaches the owner
		} 
	}

	private void returnMsg(Message msg, CHANNEL_DIRECTION channelDir) {
		msg.currentRound++;
		msg.currentProcessId = this.id;
		msg.direction =  DIRECTION.IN;
				
		if (channelDir == CHANNEL_DIRECTION.LEFT)
			ltCh.putMessage(msg);
		else if (channelDir == CHANNEL_DIRECTION.RIGHT)
			rtCh.putMessage(msg);
	}
	
	private void forwardMsg(Message msg, CHANNEL_DIRECTION channelDir) {
		msg.currentRound++;
		msg.currentProcessId = this.id;
		if (msg.direction == DIRECTION.OUT)
			msg.hopcount--;
		if (channelDir == CHANNEL_DIRECTION.LEFT) 
			rtCh.putMessage(msg);
		else if (channelDir == CHANNEL_DIRECTION.RIGHT)
			ltCh.putMessage(msg);
	}

	public void run() {
		try {
			while (true) {
				if (!isRoundComplete) {
					// Handle received messages
					Message ltChMsg = ltCh.getMessage(this.id);
					if (ltChMsg != null)
						processIncomingMessage(ltChMsg, CHANNEL_DIRECTION.LEFT);

					Message rtChMsg = rtCh.getMessage(this.id);
					if (rtChMsg != null)
						processIncomingMessage(rtChMsg, CHANNEL_DIRECTION.RIGHT);
				
					// Move to next phase
					if (returnedMessages == 2) {
						returnedMessages = 0;
						phase++;
						System.out.println("Process " + this.id + " is going to phase : " + phase);

						ltCh.putMessage(new Message(id,(int) Math.pow(2, phase) , DIRECTION.OUT, currentRound + 1));
						rtCh.putMessage(new Message(id, (int) Math.pow(2, phase), DIRECTION.OUT, currentRound + 1));
					} 
					isRoundComplete = true;;
				} else Thread.sleep(150);
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
}
