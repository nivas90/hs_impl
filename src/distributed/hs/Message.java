package distributed.hs;

public class Message {
	public int processId;
	public int hopcount;
	public DIRECTION direction;
	public int currentRound;
	public int currentProcessId;

	public Message(int processId, int hopCount, DIRECTION direction, int currentRound) {
		super();
		this.processId = processId;
		this.hopcount = hopCount;
		this.direction = direction;
		this.currentRound = currentRound;
		this.currentProcessId = processId;
	}
}
