package distributed.hs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Master {
	private static List<Thread> threads = new LinkedList<Thread>();
	private static List<Process> processRing = new LinkedList<Process>();

	private void runHS(List<Process> processes) {
		int roundNumber = 0;
		try {
			for (Thread thread : threads)	thread.start();
			
			while (true) {
				// Wait For Threads To Complete
				for (Process process : processes) {		
					while (true) {
						synchronized(process) {
							if (process.isRoundComplete == true)	break;
						}
						Thread.sleep(150);
					}
				}

				// If leader is elected, terminate all threads
				if (isLeaderElected(processes)) {
					for (Thread thread : threads) {
						try {
							thread.interrupt();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					break;
				}
				
				// Else increment round number and reset round info
				roundNumber++;
				System.out.println("Going to Round : " + roundNumber );	
				for (Process process : processes) {
					synchronized (process) {
						process.isRoundComplete = false;
						process.currentRound++;
					}
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}

	private boolean isLeaderElected(List<Process> processes) {
		for (Process process : processes) {
			if (process.leader == 1) {
				System.out.println("Process with index " + process.number +" is the leader. Process ID = " + process.id);
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		long begin = System.nanoTime();
		String 	inputFile = "conf/input_file.txt";
		Channel lChannel = null;
		Channel rChannel = null;

		int[] processIDs;
		BufferedReader br = new BufferedReader(new FileReader(inputFile));
		int numProcesses = Integer.parseInt(br.readLine());
		processIDs = new int [numProcesses];
		System.out.println("No. of Processes: " + numProcesses);
		
		for (int i = 0; i < numProcesses; i++) {
			 processIDs[i] = Integer.parseInt(br.readLine());
			 
			if(i==0)	lChannel = new Channel();
			else	lChannel = processRing.get(processRing.size() - 1).rtCh;
						
			if(i == numProcesses - 1)	rChannel = processRing.get(0).ltCh;
			else	rChannel = new Channel();

			Process process = new Process(i, processIDs[i], lChannel, rChannel);
			processRing.add(process);
		}

		for (Process process : processRing) {
			Thread thread = new Thread(process, "Process:" + process.id);
			System.out.println("Created thread for "  + thread.getName());
			threads.add(thread);
		}
		
		Master master = new Master();
		master.runHS(processRing);
		long duration = System.nanoTime() - begin;
		System.out.println("Program Duration(sec): " + TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
	}
}
