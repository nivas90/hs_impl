package distributed.hs;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Channel {
	private List<Message> messages = new LinkedList<Message>();

	public synchronized void putMessage(Message message) {
		messages.add(message);
	}

	public synchronized Message getMessage(int processId) {
		Iterator<Message> it = messages.iterator();
		while (it.hasNext()) {
			Message msg = it.next();
			if (msg.currentProcessId != processId) {
				it.remove();
				return msg;
			}
		}
		return null;
	}
}
