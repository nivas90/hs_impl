The following are the commands to compile and run the program:
javac src/distributed/hs/*.java
java -cp ./src distributed.hs.Master


The program reads its input from conf/input_file.txt

Here is a sample output:
No. of Processes: 50
Created thread for Process:99
Created thread for Process:162
Created thread for Process:165
Created thread for Process:297
Created thread for Process:168
Created thread for Process:0
Created thread for Process:3
Created thread for Process:102
Created thread for Process:105
Created thread for Process:108
Created thread for Process:156
Created thread for Process:159
Created thread for Process:6
Created thread for Process:183
Created thread for Process:186
Created thread for Process:171
Created thread for Process:174
Created thread for Process:177
Created thread for Process:180
Created thread for Process:234
Created thread for Process:237
Created thread for Process:189
Created thread for Process:192
Created thread for Process:195
Created thread for Process:198
Created thread for Process:201
Created thread for Process:210
Created thread for Process:213
Created thread for Process:216
Created thread for Process:219
Created thread for Process:222
Created thread for Process:225
Created thread for Process:258
Created thread for Process:9
Created thread for Process:12
Created thread for Process:15
Created thread for Process:18
Created thread for Process:261
Created thread for Process:264
Created thread for Process:132
Created thread for Process:135
Created thread for Process:138
Created thread for Process:141
Created thread for Process:144
Created thread for Process:66
Created thread for Process:69
Created thread for Process:72
Created thread for Process:75
Created thread for Process:78
Created thread for Process:81
Process 162 is going to phase : 1
Process 99 is going to phase : 1
Process 162 has lost to 165
Process 165 is going to phase : 1
Process 165 has lost to 297
Process 297 is going to phase : 1
Process 168 is going to phase : 1
Process 0 is going to phase : 1
Process 3 is going to phase : 1
Process 3 has lost to 102
Process 102 is going to phase : 1
Process 102 has lost to 105
Process 105 is going to phase : 1
Process 156 is going to phase : 1
Process 105 has lost to 108
Process 108 is going to phase : 1
Process 6 is going to phase : 1
Process 156 has lost to 159
Process 6 has lost to 159
Process 159 is going to phase : 1
Process 6 has lost to 183
Process 183 is going to phase : 1
Process 186 is going to phase : 1
Process 171 is going to phase : 1
Process 174 is going to phase : 1
Process 174 has lost to 177
Process 177 is going to phase : 1
Process 180 is going to phase : 1
Process 237 is going to phase : 1
Process 180 has lost to 234
Process 234 is going to phase : 1
Process 189 is going to phase : 1
Process 192 is going to phase : 1
Process 192 has lost to 195
Process 195 is going to phase : 1
Process 201 is going to phase : 1
Process 195 has lost to 198
Process 198 is going to phase : 1
Process 201 has lost to 210
Process 210 is going to phase : 1
Process 216 is going to phase : 1
Process 225 is going to phase : 1
Process 225 has lost to 258
Process 258 is going to phase : 1
Process 216 has lost to 219
Process 210 has lost to 213
Process 213 is going to phase : 1
Process 222 is going to phase : 1
Process 219 is going to phase : 1
Process 9 is going to phase : 1
Process 12 is going to phase : 1
Process 15 is going to phase : 1
Process 15 has lost to 18
Process 261 is going to phase : 1
Process 18 is going to phase : 1
Process 261 has lost to 264
Process 264 is going to phase : 1
Process 132 is going to phase : 1
Process 135 is going to phase : 1
Process 135 has lost to 138
Process 141 is going to phase : 1
Process 138 is going to phase : 1
Process 141 has lost to 144
Process 144 is going to phase : 1
Process 66 is going to phase : 1
Process 66 has lost to 69
Process 69 is going to phase : 1
Process 72 is going to phase : 1
Process 72 has lost to 75
Process 75 is going to phase : 1
Process 78 is going to phase : 1
Process 81 is going to phase : 1
Going to Round : 1
Process 0 has lost to 3
Process 213 has lost to 216
Process 222 has lost to 225
Process 9 has lost to 258
Process 219 has lost to 222
Process 75 has lost to 78
Process 78 has lost to 81
Process 108 has lost to 156
Process 12 has lost to 15
Process 18 has lost to 261
Process 132 has lost to 264
Process 159 has lost to 183
Process 183 has lost to 186
Process 171 has lost to 186
Process 81 has lost to 99
Process 171 has lost to 174
Process 99 has lost to 162
Process 132 has lost to 135
Process 177 has lost to 180
Process 234 has lost to 237
Process 189 has lost to 237
Process 138 has lost to 141
Process 66 has lost to 144
Process 168 has lost to 297
Process 189 has lost to 192
Process 69 has lost to 72
Process 198 has lost to 201
Going to Round : 2
Process 12 has lost to 258
Process 9 has lost to 12
Process 237 is going to phase : 2
Process 69 has lost to 144
Process 0 has lost to 168
Going to Round : 3
Process 258 is going to phase : 2
Process 135 has lost to 264
Process 174 has lost to 186
Process 186 is going to phase : 2
Process 192 has lost to 237
Process 144 is going to phase : 2
Process 297 is going to phase : 2
Process 3 has lost to 168
Going to Round : 4
Process 264 is going to phase : 2
Going to Round : 5
Going to Round : 6
Going to Round : 7
Process 258 is going to phase : 3
Process 237 is going to phase : 3
Going to Round : 8
Process 186 is going to phase : 3
Process 144 is going to phase : 3
Going to Round : 9
Process 258 has lost to 261
Process 297 is going to phase : 3
Going to Round : 10
Process 186 has lost to 234
Going to Round : 11
Process 264 is going to phase : 3
Process 144 has lost to 162
Going to Round : 12
Going to Round : 13
Process 144 has lost to 264
Going to Round : 14
Going to Round : 15
Going to Round : 16
Process 237 is going to phase : 4
Going to Round : 17
Going to Round : 18
Process 297 is going to phase : 4
Going to Round : 19
Going to Round : 20
Going to Round : 21
Process 264 is going to phase : 4
Going to Round : 22
Process 237 has lost to 258
Going to Round : 23
Going to Round : 24
Going to Round : 25
Going to Round : 26
Going to Round : 27
Process 264 has lost to 297
Going to Round : 28
Going to Round : 29
Going to Round : 30
Going to Round : 31
Going to Round : 32
Going to Round : 33
Going to Round : 34
Going to Round : 35
Going to Round : 36
Going to Round : 37
Going to Round : 38
Going to Round : 39
Process 297 is going to phase : 5
Going to Round : 40
Going to Round : 41
Going to Round : 42
Going to Round : 43
Going to Round : 44
Going to Round : 45
Going to Round : 46
Going to Round : 47
Going to Round : 48
Going to Round : 49
Going to Round : 50
Going to Round : 51
Going to Round : 52
Going to Round : 53
Going to Round : 54
Going to Round : 55
Going to Round : 56
Going to Round : 57
Going to Round : 58
Going to Round : 59
Going to Round : 60
Going to Round : 61
Going to Round : 62
Going to Round : 63
Going to Round : 64
Going to Round : 65
Going to Round : 66
Going to Round : 67
Going to Round : 68
Going to Round : 69
Going to Round : 70
Going to Round : 71
Going to Round : 72
Going to Round : 73
Going to Round : 74
Going to Round : 75
Going to Round : 76
Going to Round : 77
Going to Round : 78
Going to Round : 79
Process 297 is going to phase : 6
Going to Round : 80
Going to Round : 81
Going to Round : 82
Going to Round : 83
Going to Round : 84
Going to Round : 85
Going to Round : 86
Going to Round : 87
Going to Round : 88
Going to Round : 89
Going to Round : 90
Going to Round : 91
Going to Round : 92
Going to Round : 93
Going to Round : 94
Going to Round : 95
Going to Round : 96
Going to Round : 97
Going to Round : 98
Going to Round : 99
Going to Round : 100
Going to Round : 101
Process with index 3 is the leader. Process ID = 297
Program Duration(sec): 15
